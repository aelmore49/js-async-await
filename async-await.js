const getDataPromise = (num) => new Promise((resolve, reject) => {
    setTimeout(() => {
        typeof num === 'number' ? resolve(num * 2) : reject('Not a number')
    }, 2000)
})

// Setup ASYNC-AWAIT
const processData = async () => {
    let data = await getDataPromise('ABC');
    data = await getDataPromise(data);
    return data;
};

processData().then((data) => {
    console.log('Data', data)
}).catch((error) => {
    console.log('Error:', error)
})