// SETUP FETCH REQUESTS USING ASYNC-AWAIT

const getCountryFetch = async (countryCode) => {
    const response = await fetch('http://restcountries.eu/rest/v2/all', {});
    if (response.status === 200) {
        const data = await response.json()
        return data.find((c) => c.alpha2Code === countryCode)
    } else {
        throw new Error('This request failed hard!')
    }
}

const getLocation = async () => {
    const response = await fetch('http://ipinfo.io/json?token=9922d983c6dcbb', {});
    if (response.status === 200) {
        return response.json()
    } else {
        throw new Error('This request failed hard!')
    }
}


const getPuzzleAsync = async (wordCount) => {
    const response = await fetch(`http://puzzle.mead.io/puzzle?wordCount=${wordCount}`, {})
    if (response.status === 200) {
        const data = await response.json();
        return data.puzzle;
    } else {
        throw new Error('Unable to get puzzle!')
    }
}

const getCurrentCountry = async () => {
    const location = await getLocation();
    const country = await getCountryFetch(location.country)
    return country;
}